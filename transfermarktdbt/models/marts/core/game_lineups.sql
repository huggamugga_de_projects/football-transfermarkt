with game_ids as (
    select game_id from {{ ref('games') }}
), player_ids as (
    select player_id from {{ ref('players') }}
), club_ids as (
    select club_id from {{ ref('clubs') }}
), game_lineups as (
    select gl.* from {{ source('transfermarkt_source', 'game_lineups') }} gl
    where gl.game_id in (select * from game_ids)
    and gl.club_id in (select * from club_ids)
    and gl.player_id in (select * from player_ids)
)

select * from game_lineups