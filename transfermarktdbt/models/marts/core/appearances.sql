with game_ids as (
    select game_id from {{ ref('games') }}
), players as (
    select player_id, current_club_id from {{ ref('players') }}
), club_ids as (
    select club_id from {{ ref('clubs') }}
), competition_ids as (
    select competition_id from {{ ref('competitions') }}
), appearances as (
    select a.* from {{ source('transfermarkt_source', 'appearances') }} a
    where a.game_id in (select * from game_ids) 
    and a.player_id in (select player_id from players)
    and a.player_club_id in (select * from club_ids)
    and a.player_current_club_id in (select current_club_id from players)
    and a.competition_id in (select * from competition_ids)
)

select * from appearances