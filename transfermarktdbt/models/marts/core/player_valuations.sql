with player_ids as (
    select player_id from {{ ref('players') }}
), player_valuations as (
    select pv.* from {{ source('transfermarkt_source', 'player_valuations') }} pv
    where pv.player_id in (select * from player_ids)
)

select * from player_valuations