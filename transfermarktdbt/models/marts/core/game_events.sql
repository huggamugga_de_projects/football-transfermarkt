with game_ids as (
    select game_id from {{ ref('games') }}
), player_ids as (
    select player_id from {{ ref('players') }}
), club_ids as (
    select club_id from {{ ref('clubs') }}
), game_events as (
    select ge.* from {{ source('transfermarkt_source', 'game_events') }} ge
    where ge.game_id in (select * from game_ids)
    and ge.club_id in (select * from club_ids)
    and ge.player_id in (select * from player_ids)
    and (ge.player_in_id in (select * from player_ids) or ge.player_in_id is null)
    and (ge.player_assist_id in (select * from player_ids) or ge.player_assist_id is null)
)

select * from game_events