with  games as (
    select * from {{ source('transfermarkt_source', 'games') }}
)

select g.* 
from games g 
where g.home_club_id in (select club_id from {{ ref('clubs') }}) and g.away_club_id in (select club_id from {{ ref('clubs') }})