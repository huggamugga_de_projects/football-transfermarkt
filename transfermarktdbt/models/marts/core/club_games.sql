with game_ids as (
    select game_id from {{ ref('games') }}
), club_ids as (
    select club_id from {{ ref('clubs') }}
), club_games as (
    select cg.* from {{ source('transfermarkt_source', 'club_games') }} cg
    where cg.game_id in (select * from game_ids)
    and cg.club_id in (select * from club_ids)
    and cg.opponent_id in (select * from club_ids)
)

select * from club_games