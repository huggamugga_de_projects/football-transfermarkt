import findspark
import os
findspark.init(os.environ.get("PYSPARK_DIR"))

import pyspark
import boto3

from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql.window import Window
import pyspark.sql.functions as f
from datetime import datetime, date
from pyspark.sql import SQLContext
from pyspark.sql import DataFrameWriter
from pyspark.sql import DataFrame

class SourceDataFrame:
    def __init__(self, appearancesDF, clubsDF, competitionsDF, gamesDF, playersDF):
        self.appearancesDF = appearancesDF
        self.clubsDF = clubsDF
        self.competitionsDF = competitionsDF
        self.gamesDF = gamesDF
        self.playersDF = playersDF

class WarehouseDataFrame:
    def __init__(self, appearancesFactDF, clubsDimDF, competitionsDimDF, gamesDimDF, playersDimDF, datesDimDF):
        self.appearancesFactDF = appearancesFactDF
        self.clubsDimDF = clubsDimDF
        self.competitionsDimDF = competitionsDimDF
        self.gamesDimDF = gamesDimDF
        self.playersDimDF = playersDimDF
        self.datesDimDF = datesDimDF

class TransformAppearancesDFDependency:
    def __init__(self, clubsDimDF, competitionsDimDF, gamesDimDF, playersDimDF):
        self.clubsDimDF = clubsDimDF
        self.competitionsDimDF = competitionsDimDF
        self.gamesDimDF = gamesDimDF
        self.playersDimDF = playersDimDF

def extract(spark: SparkSession) -> SourceDataFrame:
    dbUrlString = os.environ.get('RDS_URL_STRING')
    dbUser = os.environ.get('RDS_USER')
    dbPassword = os.environ.get('RDS_PASSWORD')
    dbTableMap = {
        'appearances': 'transfermarkt_schema.appearances',
        'club_games': 'transfermarkt_schema.club_games',
        'clubs': 'transfermarkt_schema.clubs',
        'competitions': 'transfermarkt_schema.competitions',
        'game_events': 'transfermarkt_schema.game_events',
        'game_lineups': 'transfermarkt_schema.game_lineups',
        'games': 'transfermarkt_schema.games',
        'player_valuations': 'transfermarkt_schema.player_valuations',
        'players': 'transfermarkt_schema.players'
    }

    appearancesDF = spark.read \
        .jdbc(dbUrlString, dbTableMap['appearances'],
          properties={"user": dbUser, "password": dbPassword})
    
    clubsDF = spark.read \
        .jdbc(dbUrlString, dbTableMap['clubs'],
          properties={"user": dbUser, "password": dbPassword})
    
    competitionsDF = spark.read \
        .jdbc(dbUrlString, dbTableMap['competitions'],
          properties={"user": dbUser, "password": dbPassword})

    gamesDF = spark.read \
        .jdbc(dbUrlString, dbTableMap['games'],
          properties={"user": dbUser, "password": dbPassword})

    playersDF = spark.read \
        .jdbc(dbUrlString, dbTableMap['players'],
          properties={"user": dbUser, "password": dbPassword})
    
    return SourceDataFrame(appearancesDF, clubsDF, competitionsDF, gamesDF, playersDF)

def transform(sourceDF: SourceDataFrame) -> WarehouseDataFrame:
    datesDimDF = transformDatesDF(sourceDF.appearancesDF)
    gamesDimDF = transformGamesDF(sourceDF.gamesDF)
    competitionsDimDF = transformCompetitionsDF(sourceDF.competitionsDF)
    clubsDimDF = transformClubsDF(sourceDF.clubsDF)
    playersDimDF = transformPlayersDF(sourceDF.playersDF)

    transformAppearancesDFDep = TransformAppearancesDFDependency(clubsDimDF, competitionsDimDF, gamesDimDF, playersDimDF)
    appearancesFactDF = transformAppearancesDF(sourceDF.appearancesDF, transformAppearancesDFDep)

    gamesDimDF = removeGamesDimUnusedColumns(gamesDimDF)

    return WarehouseDataFrame(appearancesFactDF, clubsDimDF, competitionsDimDF, gamesDimDF, playersDimDF, datesDimDF)

    
def transformDatesDF(appearancesDF: DataFrame) -> DataFrame:
    datesDimDF = appearancesDF.select(
        f.column('date').alias('date_key'),
        f.year('date_key').alias('year'),
        f.month('date_key').alias('month'),
        f.dayofmonth('date_key').alias('day'),
        f.date_format('date_key', 'EEEE').alias('day_english'),
        f.date_format('date_key', 'MMMM').alias('month_english'),
        f.quarter('date_key').alias('quarter')
    ).dropDuplicates(["date_key"])

    return datesDimDF
    
def transformGamesDF(gamesDF: DataFrame) -> DataFrame:
    tempPartition = Window.partitionBy('temp').orderBy('temp')
    
    gamesDimColumns = [
        'game_id',
        'season',
        'round',
        'home_club_goals',
        'away_club_goals',
        'aggregate',
        'home_club_position',
        'away_club_position',
        'stadium',
        'attendance',
        'referee',
        'url',
        'home_club_id',
        'away_club_id'
    ]

    gamesDimDF = gamesDF.select(*gamesDimColumns).sort('game_id', ascending=[True]).withColumn('temp', f.lit('ABC'))
    gamesDimDF = gamesDimDF.withColumn('game_key', f.row_number().over(tempPartition)).drop('temp')

    return gamesDimDF

def transformCompetitionsDF(competitionsDF: DataFrame) -> DataFrame:
    tempPartition = Window.partitionBy('temp').orderBy('temp')
    
    competitionsDimColumns = [
        'competition_id',
        'competition_code',
        'name',
        'type',
        'sub_type',
        'country_id',
        'country_name',
        'domestic_league_code',
        'confederation',
        'url'
    ]

    competitionsDimDF = competitionsDF.select(*competitionsDimColumns).sort('competition_id', ascending=[True]).withColumn('temp', f.lit('ABC'))
    competitionsDimDF = competitionsDimDF.withColumn('competition_key', f.row_number().over(tempPartition)).drop('temp')

    return competitionsDimDF

def transformClubsDF(clubsDF: DataFrame) -> DataFrame:
    tempPartition = Window.partitionBy('temp').orderBy('temp')

    clubsDimColumns = [
        'club_id',
        'club_code',
        'name',
        'total_market_value',
        'average_age',
        'foreigners_number',
        'foreigners_percentage',
        'national_team_players',
        'stadium_name',
        'stadium_seats',
        'net_transfer_record',
        'coach_name',
        'url',
        'squad_size'
    ]

    clubsDimDF = clubsDF.select(*clubsDimColumns).sort('club_id', ascending=[True]).withColumn('temp', f.lit('ABC'))
    clubsDimDF = clubsDimDF.withColumn('club_key', f.row_number().over(tempPartition)).drop('temp')

    return clubsDimDF

def transformPlayersDF(playersDF: DataFrame) -> DataFrame:
    tempPartition = Window.partitionBy('temp').orderBy('temp')

    playersDimColumns = [
        'player_id',
        'country_of_citizenship',
        'country_of_birth',
        'city_of_birth',
        'date_of_birth',
        'position',
        'sub_position',
        'foot',
        'height_in_cm',
        'market_value_in_eur',
        'highest_market_value_in_eur',
        'agent_name',
        'contract_expiration_date',
        'first_name',
        'last_name',
        'player_code',
        'image_url',
        'last_season',
        'url'
    ]

    playersDimDF = playersDF.select(*playersDimColumns).sort('player_id', ascending=[True]).withColumn('temp', f.lit('ABC'))
    playersDimDF = playersDimDF.withColumn('player_key', f.row_number().over(tempPartition)).drop('temp')

    return playersDimDF

def transformAppearancesDF(appearancesDF: DataFrame, dep: TransformAppearancesDFDependency) -> DataFrame:
    appearancesFactInitialColumns = [
        'player_id',
        'player_club_id',
        'competition_id',
        'goals',
        'assists',
        'minutes_played',
        'yellow_cards',
        'red_cards',
        'date',
        'game_id'
    ]

    appearancesFactDF = appearancesDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.join(dep.playersDimDF, ['player_id'], "inner")
    appearancesFactInitialColumns.append('player_key')
    appearancesFactDF = appearancesFactDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.join(dep.competitionsDimDF, ['competition_id'], "inner")
    appearancesFactInitialColumns.append('competition_key')
    appearancesFactDF = appearancesFactDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.join(dep.gamesDimDF, ['game_id'], "inner")
    appearancesFactInitialColumns = appearancesFactInitialColumns + ['game_key','home_club_id','away_club_id']
    appearancesFactDF = appearancesFactDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.join(dep.clubsDimDF, appearancesFactDF.player_club_id == dep.clubsDimDF.club_id, "inner")
    appearancesFactDF = appearancesFactDF.withColumnRenamed('club_key', 'player_club_key')
    appearancesFactInitialColumns.append('player_club_key')
    appearancesFactDF = appearancesFactDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.join(dep.clubsDimDF, appearancesFactDF.home_club_id == dep.clubsDimDF.club_id, "inner")
    appearancesFactDF = appearancesFactDF.withColumnRenamed('club_key', 'home_club_key')
    appearancesFactInitialColumns.append('home_club_key')
    appearancesFactDF = appearancesFactDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.join(dep.clubsDimDF, appearancesFactDF.away_club_id == dep.clubsDimDF.club_id, "inner")
    appearancesFactDF = appearancesFactDF.withColumnRenamed('club_key', 'away_club_key')
    appearancesFactInitialColumns.append('away_club_key')
    appearancesFactDF = appearancesFactDF.select(*appearancesFactInitialColumns)

    appearancesFactDF = appearancesFactDF.drop('home_club_id','away_club_id','player_id','player_club_id','competition_id','game_id').withColumnRenamed('date','date_key')

    return appearancesFactDF

def load(spark: SparkSession, whDF: WarehouseDataFrame): 
    s3TargetBucket = os.environ.get('S3_TARGET_BUCKET')

    spark.sparkContext._jsc.hadoopConfiguration().set("fs.s3a.access.key", os.environ.get('AWS_ACCESS_KEY'))
    spark.sparkContext._jsc.hadoopConfiguration().set("fs.s3a.secret.key", os.environ.get('AWS_SECRET_KEY'))
    spark.sparkContext._jsc.hadoopConfiguration().set("fs.s3a.endpoint", "s3.amazonaws.com")

    whDF.datesDimDF.write\
    .option("header","true")\
    .mode("overwrite")\
    .csv("s3a://" + s3TargetBucket + "/wh/dates_dim")

    whDF.competitionsDimDF.write\
    .option("header","true")\
    .mode("overwrite")\
    .csv("s3a://" + s3TargetBucket + "/wh/competitions_dim")

    whDF.gamesDimDF.write\
    .option("header","true")\
    .mode("overwrite")\
    .csv("s3a://" + s3TargetBucket + "/wh/games_dim")

    whDF.clubsDimDF.write\
    .option("header","true")\
    .mode("overwrite")\
    .csv("s3a://" + s3TargetBucket + "/wh/clubs_dim")

    whDF.playersDimDF.write\
    .option("header","true")\
    .mode("overwrite")\
    .csv("s3a://" + s3TargetBucket + "/wh/players_dim", quoteAll=True)

    whDF.appearancesFactDF.write\
    .option("header","true")\
    .mode("overwrite")\
    .csv("s3a://" + s3TargetBucket + "/wh/appearances_fact")

def removeGamesDimUnusedColumns(gamesDimDF: DataFrame) -> DataFrame:
    gamesDimDF = gamesDimDF.drop('home_club_id','away_club_id')
    return gamesDimDF


if __name__ == "__main__":
    # $example on:init_session$
    spark = SparkSession\
        .builder\
        .getOrCreate()
    
    sourceDF = extract(spark)
    whDF = transform(sourceDF)
    load(spark, whDF)