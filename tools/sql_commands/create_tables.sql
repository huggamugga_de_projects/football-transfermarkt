BEGIN;

DROP SCHEMA IF EXISTS transfermarkt_source_schema CASCADE;

CREATE SCHEMA IF NOT EXISTS transfermarkt_source_schema;

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.appearances (
    appearance_id text PRIMARY KEY,
    game_id int,
    player_id int,
    player_club_id int,
    player_current_club_id int,
    date date,
    player_name text,
    competition_id text,
    yellow_cards int,
    red_cards int,
    goals int,
    assists int,
    minutes_played int
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.club_games (
    game_id int,
    club_id int,
    own_goals int,
    own_position int,
    own_manager_name text,
    opponent_id int,
    opponent_goals int,
    opponent_position int,
    opponent_manager_name text,
    hosting text,
    is_win int
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.clubs (
    club_id int PRIMARY KEY,
    club_code text,
    name text,
    domestic_competition_id text,
    total_market_value int,
    squad_size int,
    average_age numeric,
    foreigners_number int,
    foreigners_percentage numeric,
    national_team_players int,
    stadium_name text,
    stadium_seats int,
    net_transfer_record text,
    coach_name text,
    last_season int,
    url text
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.competitions (
    competition_id text PRIMARY KEY,
    competition_code text,
    name text,
    sub_type text,
    type text,
    country_id int,
    country_name text,
    domestic_league_code text,
    confederation text,
    url text
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.game_events (
    game_event_id text PRIMARY KEY,
    date date,
    game_id int,
    minute int,
    type text,
    club_id int,
    player_id int,
    description text,
    player_in_id int,
    player_assist_id int
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.game_lineups (
    game_lineups_id text PRIMARY KEY,
    game_id int,
    club_id int,
    type text,
    number int,
    player_id int,
    player_name text,
    team_captain int,
    position text
);


CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.games (
    game_id integer PRIMARY KEY,
    competition_id text,
    season integer,
    round text,
    date date,
    home_club_id integer,
    away_club_id integer,
    home_club_goals integer,
    away_club_goals integer,
    home_club_position integer,
    away_club_position integer,
    home_club_manager_name text,
    away_club_manager_name text,
    stadium text,
    attendance integer,
    referee text,
    url text,
    home_club_formation text,
    away_club_formation text,
    home_club_name text,
    away_club_name text,
    aggregate text,
    competition_type text
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.players (
    player_id integer PRIMARY KEY,
    first_name text,
    last_name text,
    name text,
    last_season int,
    current_club_id int,
    player_code text,
    country_of_birth text,
    city_of_birth text,
    country_of_citizenship text,
    date_of_birth date,  -- Assuming date_of_birth is a date
    sub_position text,
    position text,
    foot text,
    height_in_cm int,
    market_value_in_eur int,
    highest_market_value_in_eur int,
    contract_expiration_date timestamp,
    agent_name text,
    image_url text,
    url text,
    current_club_domestic_competition_id text,
    current_club_name text
);

CREATE TABLE IF NOT EXISTS transfermarkt_source_schema.player_valuations (
    player_id int,
    last_season int,
    datetime timestamp,
    date date,
    dateweek date,
    market_value_in_eur int,
    n int,
    current_club_id int,
    player_club_domestic_competition_id text
);

COMMIT;