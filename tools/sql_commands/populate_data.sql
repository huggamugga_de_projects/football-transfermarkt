BEGIN;

COPY transfermarkt_source_schema.appearances
FROM '/football_transfermarkt/appearances.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.club_games
FROM '/football_transfermarkt/club_games.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.clubs
FROM '/football_transfermarkt/clubs.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.competitions
FROM '/football_transfermarkt/competitions.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.game_events
FROM '/football_transfermarkt/game_events.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.game_lineups
FROM '/football_transfermarkt/game_lineups.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.games
FROM '/football_transfermarkt/games.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.players
FROM '/football_transfermarkt/players.csv'
DELIMITER ','
CSV HEADER;

COPY transfermarkt_source_schema.player_valuations
FROM '/football_transfermarkt/player_valuations.csv'
DELIMITER ','
CSV HEADER;

COMMIT;